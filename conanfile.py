from conans import ConanFile, CMake, tools
import shutil

class FrozenConan(ConanFile):
    name           = "frozen"
    version        = "1.0.0"
    license        = "Apache 2.0"
    url            = "https://bitbucket.org/toge/conan-frozen"
    description    = "a header-only, constexpr alternative to gperf for C++14 users "
    no_copy_source = True

    def source(self):
        tools.get("https://github.com/serge-sans-paille/frozen/archive/{}.zip".format(self.version))
        shutil.move("frozen-{}".format(self.version), "frozen")

    def package(self):
        self.copy("*.h", src="frozen/include", dst="include")

    def package_info(self):
        self.info.header_only()
