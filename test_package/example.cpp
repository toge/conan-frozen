#include <cstdlib>
#include <iostream>

#include "frozen/set.h"

int main(int argc, const char** argv) {
    constexpr auto some_ints = frozen::make_set<int>({1, 2, 3, 5});
    constexpr auto letitgo = some_ints.count(8);

    static_assert(letitgo == 0, "success frozen!");

    return 0;
}
